var orderButton = document.querySelector(".order-button");
var orderPopup = document.querySelector(".modal-form");
var closeButton = orderPopup.querySelector(".modal-form__close");
var placeOrderButton = orderPopup.querySelector(".modal__place-order");
var thanksPopup = document.querySelector(".modal-thanks");
var closePopupButton = thanksPopup.querySelector(".modal-thanks__close");

orderButton.addEventListener("click", function (evt) {
    evt.preventDefault();
    orderPopup.classList.toggle("modal-show");
});

closeButton.addEventListener("click", function (evt) {
    evt.preventDefault();
    orderPopup.classList.remove("modal-show");
});

placeOrderButton.addEventListener("click", function (evt) {
    evt.preventDefault();
    orderPopup.classList.remove("modal-show");
    thanksPopup.classList.add("modal-show");
});

closePopupButton.addEventListener("click", function (evt) {
    evt.preventDefault();
    thanksPopup.classList.remove("modal-show");
});


